
import random
import numpy as np
class Encoding():
    def __init__(self,function, encoding_size = 2):
        self.function = function
        self.encoding_size = encoding_size
        self.gene = self.random_gene()
    def random_gene(self):
        x = random.random()*(6)-3
        y = random.random()*(4)-2
        return [x,y]

    def setGen(self,gene):
        self.gene = gene

    def fitness(self):
        return self.function(self.gene)

    def cross_over(self, slected_parents, mutation_p):
        # Add some randomness to cross_over
        if random.random() < 0.5:
            self.gene = [self.gene[:][0]]+[slected_parents.gene[:][1]]
        else:
            self.gene = [slected_parents.gene[:][0]]+ [self.gene[:][1]]
        # muation
        if random.random() < mutation_p:
            idx = np.random.choice(range(self.encoding_size))
            if idx == 0:
                self.gene[0] = random.random()*(6)-3
            else:
                self.gene[1] = random.random()*(4)-2
        return self.gene
