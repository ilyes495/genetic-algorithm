import numpy as np
import random
import population
import math

def func():
    def objective(xy):
        x,y = xy[0],xy[1]
        z = (4- 2.1 * x**2 + x**4/3)* x**2 + x*y +(-4+4*y**2)* y**2
        return z
    return objective

class GA():
    def __init__(self,N_generations, size_population,z_min, mutation_p=0.1 ):
        self.mutation_p = mutation_p
        self.size_population = size_population
        self.N_generations = N_generations
        self.z_min = z_min

        f = func()
        self.population = population.Population(size_population, f) #N,function,n_select = 2, selection_mode =''

    def draw(self, generation):

        self.population.calcfitness()
        #self.population.naturalSelction()
        self.population.generate(self.mutation_p)
        if generation%1 ==0 :
            self.population.evaluate()

    def converged(self):
        z_ = self.population.evaluate_()
        return math.isclose(self.z_min, z_, rel_tol=0.01)


if __name__ == '__main__':
    # Size of the population
    P = 1000
    # Number of generation
    G= 10
    f = func()
    z_min = f([0.090,-0.715])
    GA = GA(G,P, z_min)

    for generation in range(G):
        GA.draw(generation)
        if GA.converged():
            print('converged after {} generations'.format(generation))
            GA.population.evaluate()
            break

    #print(GA.population.fitness)
    # best_xy = GA.best_xy()
    # best_z = GA.best_xy()
