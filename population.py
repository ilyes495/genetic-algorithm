import random
import numpy as np
import Encoding

class Population():
    def __init__(self,N,function,n_select = 2, selection_mode =''):
        self.N = N
        self.n_select = n_select
        self.function = function
        self.population = [Encoding.Encoding(self.function) for _ in range(self.N)]
        self.fitness = [0]*N

    def calcfitness(self):
        self.fitness = [p.fitness() for p in self.population]
        return self.fitness

    # def naturalSelction(self,n_select = 2):
    #     self.select = list(np.random.choice(range(N),size =n_select, p =(1/self.fitness)/sum(self.fitness) ))
    #     retun self.select

    def generate(self, mutation_p):

        # The new population will have half as offspring, and half as the best paraents
        # the offspring will be a cross over of the N/2 paretns
        new_population = []
        # get the indeicies of the max chromosomes
        self.fitness = np.asarray(self.fitness)
        #print(, len(self.fitness))
        K = int(self.N/2)
        max_indx = self.fitness.argsort()[:K]
        #print(max_indx,self.fitness[max_indx])
        self.population = np.asarray(self.population)
        parents = self.population[max_indx]
        new_children = parents[:]
        for i in range(len(parents)):
            new_children[i].cross_over(parents[i%len(parents)], mutation_p)

        new_population = []
        for i in range(K):
            #print(i)
            new_population.append(new_children[i])
            new_population.append(parents[i])


        self.population = new_population

        # #print('p: ', p)
        # p = p - max(p)
        # #print('p-min(p): ',p)
        # p *= -1
        # #print('p*-1: ',p)
        #
        #
        # #print('p - min(p): ',p)
        # #p /= (np.max(p)- np.min(p))
        # p /= sum(p)
        # #print('p /= sum(p): ',p)
        #
        #
        #
        #
        # for i in range(self.N):
        #     #try:
        #
        #
        #     # print(p.shape)
        #     # print(p)
        #     # print(sum(p))
        #     self.select = list(np.random.choice(range(self.N),size =self.n_select, p = p))
        #     #print(len(self.select))
        #     # except:
        #     #     print(fits,'\n', p, sum(p))
        #     #     self.select
        #
        #
        #     P_a = self.population[self.select[0]]
        #     P_b = self.population[self.select[1]]
        #
        #     P_a.cross_over(P_b, mutation_p)
        #     new_population.append(P_a)
        #     #[p.cross_over(self.select, mutation_p) for p in self.population]
        # self.population = new_population


    def evaluate(self):
        min_z  = np.argmin(self.fitness)
        gene = self.population[min_z].gene
        z = self.function(gene)
        print ("Best of the population is x: {:.2f}, y:{:.2f}, z:{:.2f}".format(gene[0], gene[1], z))
    def evaluate_(self):
        min_z  = np.argmin(self.fitness)
        gene = self.population[min_z].gene
        return self.function(gene)
